#!/home/aetherai/.pyenv/shims/python
import argparse
import os
import string
import sys


def parse_arguments() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        prog=os.path.basename(__file__),
        usage=f"""
    {os.path.basename(__file__)} -H TASK_ID TASK_NAME

    Spaces are allowed in TASK_NAME while non-alphanumeric characters are ignored.

example:
    {os.path.basename(__file__)} -H 86enwfg64 Server responses 404 every full moon
""",
    )
    parser.add_argument('-H', '--hash', required=True, help='Task ID')
    parser.add_argument('task_name', nargs='+')
    return parser.parse_args()


def main():
    args = parse_arguments()
    invalid_symbols = ''.join([s for s in string.punctuation if s not in '-_'])
    invalid_trans = str.maketrans('', '', invalid_symbols)
    task_name = '-'.join([token.translate(invalid_trans) for token in args.task_name if token.isascii()])
    task_id = args.hash
    print(
        f'git checkout -b {task_name}__CU-{task_id}',
        end='\n' if os.isatty(sys.stdout.fileno()) else '',
    )


if __name__ == '__main__':
    main()
