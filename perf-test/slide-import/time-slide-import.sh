#!/bin/bash

set -e

while getopts "n:h:k:o:" opt; do
    case "$opt" in
        n) n="$OPTARG";;
        h) host="$OPTARG";;
        k) session_id="$OPTARG";;
        \?) exit 1;;
    esac
done

if [ -z "${n+x}" ] || [ -z "${host+x}" ] || [ -z "${session_id+x}" ]; then
    echo -e "Usage:\n\tbash $0 -n <N_TARGETS> -h <HOST> -k <SESSION_ID>" >&2
    echo -e "Example:\n\tbash $(basename $0 .sh).sh -n 100 -h https://10.1.0.243/ -k 2fo3djbsthwjaas494fl8eih47790ado" >&2
    exit 1
fi

host="${host%/}"
start_time=$(date +%s)
orig_count=$(curl -s "$host/home/viewer/paged_slide" -H "cookie: website-sessionid=$session_id" --insecure | jq -r ".count")
while true; do
    count=$(curl -s "$host/home/viewer/paged_slide" -H "cookie: website-sessionid=$session_id" --insecure | jq -r ".count")
    imported=$((count - orig_count))

    current_time=$(date +%s)
    echo -ne "\r$imported slides imported. $((current_time - start_time))s elapsed... "

    if [ $imported -eq $n ]; then
        echo ""
        break
    fi
    sleep 1
done

echo "Done!"

