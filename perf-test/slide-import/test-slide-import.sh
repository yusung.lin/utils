#!/bin/bash

set -e

while getopts "n:s:d:h:k:t:" opt; do
    case "$opt" in
        n) n=$OPTARG;;
        s) slide="$OPTARG";;
        d) auto_import_path="$OPTARG";;
        h) host="$OPTARG";;
        k) session_key="$OPTARG";;
        t) iterations=$OPTARG;;
        \?) exit 1;;
    esac
done

iterations=${iterations:-3}

if [ -z ${n+x} ] || [ -z "${slide+x}" ] || [ -z "${auto_import_path+x}" ] || [ -z "${host+x}" ] || [ -z "${session_key+x}" ]; then
    echo -e "Usage:\n\t$0 -s <SLIDE> -n <N_SLIDES> -d <AUTO_IMPORT_PATH> -h <HOST> -z <SESSION_KEY> [-t <TIMES>]" >&2
    echo -e "Example:\n\tbash $(basename $0 .sh).sh -s /tmp/test.isyntax -n 3 -d ~/website/data/storage/auto_import -h https://10.1.0.243 -k 2fo3djbsthwjaas494fl8eih47790ado" >&2
    exit 1
fi

if [ ! -f "$slide" ]; then
    echo "Slide '$slide' not found." >&2
    exit 1
fi

if [ ! -d "$auto_import_path" ]; then
    echo "Auto-import path '$auto_import_path'." >&2
    exit 1
fi

if command -v openssl >/dev/null 2>&1; then
    hashed="$(openssl rand -hex 4)"
else
    hashed="$(echo $RANDOM | md5sum | cut -c 1-8)"
fi

for ((i=1; i<=$iterations; i++)); do
    test_dir="/tmp/perf-test-$hashed-$i-$n"
    test_slide="/tmp/perf-test-$hashed-$i-$n.${slide##*.}"
    mkdir "$test_dir"
    cp "$slide" "$test_slide"
    bash copy-slide.sh -n $n -s "$test_slide" -d "$test_dir" --use-link
    rm -f "$test_slide"

    mv "$test_dir" "$auto_import_path" && bash time-slide-import.sh -n $n -h $host -k "$session_key"
done

