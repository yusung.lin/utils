#!/bin/bash

set -e

while [[ $# -gt 0 ]]; do
    case "$1" in
        --number|-n)
            n="$2"
            shift 2
            ;;

        --source|-s)
            src="$2"
            shift 2
            ;;

        --destination|-d)
            dest="$2"
            shift 2
            ;;

        --use-link|-l)
            use_link=true
            shift 1
            ;;

        *)
            echo "Unknown option: $1"
            exit
            ;;
    esac
done

if [ -z "${n+x}" ] || [ -z "${src+x}" ] || [ -z "${dest+x}" ]; then
    echo -e "Usage:\n\tbash $0 -n|--number <COPIES> -s|--source <FILE> -d|--destination <DIRECTORY>"
    echo -e "Example:\n\tbash $(basename $0 .sh).sh -n 3 -s test.isyntax -d ~/website/data/storage/auto_import/"
    exit 1
fi

if [ ! -f "$src" ]; then
    echo "Source file '$src' does not exist."
    exit 1
fi

if [ ! -d "$dest" ]; then
    echo "Destination directory '$dest' does not exist."
    exit 1
fi

ext="${src##*.}"
stem="$(basename $src .$ext)"
for ((i=1; i<=$n; i++)); do
    if [ -z $use_link ]; then
        cp "$src" "$dest/$stem-$i.$ext"
    else
        ln "$src" "$dest/$stem-$i.$ext"
    fi
done
echo "Created $n copies of $(basename $src) to $dest"
