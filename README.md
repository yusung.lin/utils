# Overview

The project link is https://gitlab.com/yusung.lin/utils/ and the raw file can be accessed via /raw/BRANCH/FILE. Thus, any script can be downloaded directly via

```bash
sudo curl -o /usr/local/bin/git-brgen https://gitlab.com/yusung.lin/utils/-/raw/master/git-brgen.py
sudo chmod +x /usr/local/bin/git-brgen
git-brgen --help
```
